Githut-Resume App

This application uses Spring boot, and AngularJS.

The application is written in intellij in case you want to open and check the code.

For simplicity of running, there is a "jar" folder that you can run the application with that. 

After running just go to "localhost:8080"

Or Alternatively you can check the app at this address:

https://resumegithub.herokuapp.com/


Unauthenticated API-Calls to Github can be done only 60 times in an hour, therefore if that exceeds, this error will be shown:

"Github-API call exceeded 60"