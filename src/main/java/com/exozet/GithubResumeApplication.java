package com.exozet;

import configuration.SpringConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;


public class GithubResumeApplication {


	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringConfiguration.class);
		builder.headless(false);
		ConfigurableApplicationContext applicationContext = builder.run(args);
	}
}
