package service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.ApiResponse;
import model.RepositoryLanguages;
import model.Repositories;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by emadnikkhouy on 28/03/16.
 */

@Component
public class GithubRest {

    @Value("${githubUsersUrl}")
    private String githubUsersUrl;
    @Value("${githubReposUrl}")
    private String githubReposUrl;
    @Autowired
    private RestTemplate restTemplate;
    private List<Repositories> repositories;
    private List<JSONObject> languages = new ArrayList<>();

    public List<RepositoryLanguages> getUserRepositories(String username) throws HttpClientErrorException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String response = restTemplate.getForObject(githubUsersUrl + username + "/repos", String.class);
            repositories = mapper.readValue(response, new TypeReference<List<Repositories>>() {
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
        getRepositoryLanguages(username);

        return getRepositoriesWithLanguages();
    }


    private void getRepositoryLanguages(String username) {
        for (int i = 0; i < repositories.size(); i++) {
            String response = restTemplate.getForObject(githubReposUrl + username + "/" + repositories.get(i).getName() + "/languages", String.class);
            languages.add(new JSONObject(response));
        }

    }

    private List<String> getLanguageKeys() {
        String tempKeys = "";
        List<String> keyList = new ArrayList<>();
        for (int i = 0; i < repositories.size(); i++) {
            Iterator<?> keys = languages.get(i).keys();
            while (keys.hasNext()) {
                tempKeys += keys.next() + " ";
            }
            if (tempKeys.isEmpty()) {
                keyList.add("No Language Reported");
            } else {
                keyList.add(tempKeys);
                tempKeys = "";
            }
        }
        return keyList;
    }

    private List<RepositoryLanguages> getRepositoriesWithLanguages() {
        List<String> keys = getLanguageKeys();
        List<RepositoryLanguages> repositoryLanguages = new ArrayList<>();
        for (int i = 0; i < repositories.size(); i++)
            repositoryLanguages.add(new RepositoryLanguages(repositories.get(i).getName(), keys.get(i)));
        return repositoryLanguages;
    }

}
