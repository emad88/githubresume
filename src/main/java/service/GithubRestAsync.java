package service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Repositories;
import model.RepositoryLanguages;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by emadnikkhouy on 02/04/16.
 */
@Component
public class GithubRestAsync {

    @Value("${githubUsersUrl}")
    private String githubUsersUrl;
    @Value("${githubReposUrl}")
    private String githubReposUrl;
    @Autowired
    private AsyncRestTemplate restTemplate;
    @Autowired
    private HttpHeaders httpHeaders;
    private List<Repositories> repositories;


    public List<ListenableFuture> getRepositoryLanguages(String username) throws RuntimeException{
      return getRepositoryLanguages(username, getUserRepositoriesFuture(username));
    }

    private ListenableFuture getUserRepositoriesFuture(String username) throws RuntimeException {
        HttpEntity entity = new HttpEntity(httpHeaders);
        ListenableFuture future = restTemplate.exchange(githubUsersUrl + username + "/repos", HttpMethod.GET, entity, String.class);
        while(!future.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        return future;
    }


    private List<ListenableFuture> getRepositoryLanguages(final String username, ListenableFuture<ResponseEntity<String>> future) throws RuntimeException {
            // final ListenableFuture<List<ListenableFuture>> futures;
            final List<ListenableFuture> futureList = new ArrayList<>();
            future.addCallback(new ListenableFutureCallback<ResponseEntity<String>>() {
                @Override
                public void onSuccess(ResponseEntity<String> response) {
                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        repositories = mapper.readValue(response.getBody(), new TypeReference<List<Repositories>>() {
                        });
                        HttpEntity entity = new HttpEntity(httpHeaders);
                        System.out.println("Repo size: " + repositories.size());
                        for (int i = 0; i < repositories.size(); i++) {
                            futureList.add(restTemplate.exchange(githubReposUrl + username + "/" + repositories.get(i).getName() + "/languages", HttpMethod.GET, entity, String.class));

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Throwable throwable) {
                    throw new RuntimeException(throwable);

                }
            });

            return futureList;
    }

    public RepositoryLanguages getRepositoriesWithLanguages(String response, int index) {
        String keys = getLanguageKeys(new JSONObject(response));
        //repositoryLanguages.add(new RepositoryLanguages(repositories.get(index).getName(), keys));
        return new RepositoryLanguages(repositories.get(index).getName(), keys);
    }


    private String getLanguageKeys(JSONObject language) {
        String tempKeys = "";
        Iterator<?> keys = language.keys();
        while (keys.hasNext()) {
            tempKeys += keys.next() + " ";
        }
        if (tempKeys.isEmpty()) {
            tempKeys = "No Language Reported";
        }

        return tempKeys;
    }


}
