package model;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by nikkhouy on 29.3.2016.
 */
@Component
public class ApiResponse {

    private List<RepositoryLanguages> value;
    private int responseCode;

    public List<RepositoryLanguages> getValue() {
        return value;
    }

    public void setValue(List<RepositoryLanguages> value) {
        this.value = value;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
