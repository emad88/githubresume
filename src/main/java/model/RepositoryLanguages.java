package model;

/**
 * Created by emadnikkhouy on 28/03/16.
 */
public class RepositoryLanguages {

    private String repositoryName;
    private String repositoryLanguages;

    public RepositoryLanguages(String repositoryName, String repositoryLanguages) {
        this.repositoryName = repositoryName;
        this.repositoryLanguages = repositoryLanguages;
    }

    public String getRepositoryName() {
        return repositoryName;
    }


    public String getRepositoryLanguages() {
        return repositoryLanguages;
    }


}
