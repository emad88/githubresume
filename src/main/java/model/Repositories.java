package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by emadnikkhouy on 28/03/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Repositories {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
