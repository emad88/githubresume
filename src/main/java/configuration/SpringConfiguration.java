package configuration;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emadnikkhouy on 28/03/16.
 */
@SpringBootApplication
@ComponentScan("controller, service,model")
@PropertySource("application.properties")
public class SpringConfiguration {

    @Bean
    public static PropertySourcesPlaceholderConfigurer getPropertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();

    }

    @Bean
    @Scope("prototype")
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Scope("prototype")
    public AsyncRestTemplate getAsyncRestTemplate() {
        return new AsyncRestTemplate();
    }


    @Bean
    @Scope("prototype")
    public HttpHeaders getHttpHeaders() {
        return new HttpHeaders();
    }

}

