package controller;

import model.ApiResponse;
import model.RepositoryLanguages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.async.DeferredResult;
import service.GithubRest;
import service.GithubRestAsync;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by emadnikkhouy on 28/03/16.
 */
@RestController
public class GithubController {

    @Autowired
    private GithubRest githubRest;
    @Autowired
    private ApiResponse result;
    @Autowired
    private GithubRestAsync githubRestAsync;

    @RequestMapping(value = "/repositories", method = RequestMethod.GET)
    private DeferredResult<ApiResponse> getUsername(@RequestParam(value = "username") String username) {
        System.out.println(username);
        final DeferredResult<ApiResponse> deferredResult = new DeferredResult<>();
        final List<RepositoryLanguages> repositoryLanguages = new ArrayList<>();
        clearLists(repositoryLanguages);
        try {
            List<ListenableFuture> futureList = githubRestAsync.getRepositoryLanguages(username);
            result.setResponseCode(200);
            for (int i = 0; i < futureList.size(); i++) {
                final int finalI = i;
                futureList.get(i).addCallback(new ListenableFutureCallback<ResponseEntity<String>>() {
                    @Override
                    public void onSuccess(ResponseEntity<String> response) {
                        repositoryLanguages.add(githubRestAsync.getRepositoriesWithLanguages(response.getBody(), finalI));

                        System.out.println(repositoryLanguages.get(finalI).getRepositoryName() + " " + repositoryLanguages.get(finalI).getRepositoryLanguages());
                        result.setValue(repositoryLanguages);
                        deferredResult.setResult(result);
                        System.out.println("defered:  " + deferredResult.getResult());
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        System.out.println("Error in the controller: " + throwable.getMessage());
                    }
                });
            }
        } catch (RuntimeException ex) {
            Throwable ee = ex.getCause ();
            if(ee instanceof HttpClientErrorException){
                result.setValue(null);
                result.setResponseCode(((HttpClientErrorException) ee).getStatusCode().value());
                deferredResult.setResult(result);
            }


        }


        return deferredResult;

    }

    private void clearLists(List<RepositoryLanguages> repositoryLanguages) {
        repositoryLanguages.clear();
    }

//    @RequestMapping(value = "/repositories", method = RequestMethod.GET)
//    private ApiResponse getUsername(@RequestParam(value = "username") String username) {
//        System.out.println(username);
//        try{
//            result.setValue(githubRest.getUserRepositoriesFuture(username));
//            result.setResponseCode(200);
//        } catch (HttpClientErrorException ex){
//            result.setValue(null);
//            result.setResponseCode(ex.getStatusCode().value());
//        }
//        return result;
//
//    }


}
