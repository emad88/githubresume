app.controller("controller", function ($scope, $http, $window, sharedProperties, $log, usSpinnerService) {
    $scope.placeHolder = "Enter a Github username...";
    $scope.generate = function () {
        usSpinnerService.spin('spinner-1');
        $http.get('repositories', {params: {username: $scope.username}}).then(function mySucces(response) {
            var responseCode = response.data.responseCode;
            //$log.debug("Data: " + response.data);
            if (responseCode != 200) {
                usSpinnerService.stop('spinner-1');
                $scope.username = "";
                if (responseCode == 404) {
                    $scope.placeHolder = "Wrong username! Try Again";
                } else if (responseCode == 403) {
                    $scope.placeHolder = "Github-API call exceeded 60";
                } else {
                    $scope.placeHolder = "Unknown Error!";
                }
            } else if (responseCode == 200) {
                sharedProperties.setData(response.data.value);
                $window.location.href = '/result.html' + '#/?username=' + $scope.username;
            }
        }, function myError(response) {
            usSpinnerService.stop('spinner-1');
            $scope.username = "";
            $scope.placeHolder = "Something went wrong, try later";
        });
    };
    $scope.about = function () {
        $window.location.href = '/about.html';
    }

});

app.controller("resultController", function ($scope, $window, sharedProperties, $location) {
    var data = sharedProperties.getData();
    var username = $location.search().username;
    $scope.username = username;
    $scope.names = data;
    $scope.home = function () {
        $window.location.href = '/index.html';
    }
});

app.controller("aboutController", function ($scope, $window) {
    $scope.home = function () {
        $window.location.href = '/index.html';
    }
});